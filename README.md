# Affiliate
This project is used for update FILPAL latest announcement and communication. Currently only applied in Viewer desktop application. `feed.json` is a source feed to push latest news.

# Image size
The image size should be compressed to **800x460** px only

## Feed
The file can contains empty, one, or more entries, and each feed must contains all syntax below except `url`. If not the info tab will fail to display correctly.

Last entry must not ended with comma.

- type: It can be "announcement", or "promotion".
- url: (optional) Put a visible reference link in end of body text. i.e. see www.filpal.com. This can be empty if you don't have any link availble to show typeically just an important message.
- redirect: A full url path to open in browser  when user click on it. Usually this is a long url and will not display in news feed. It will display the value in `url` if there is any.
- description: Display text or message. Try to be short or not more than 60-80 characters long due to default Window size is the constraint unless maximize.
- body (obsolete): Display text or message. Try to be short or not more than 60-80 characters long due to default Window size is the constraint unless maximize.

Body message must maintain in below format otherwise the clickable url will not display correctly under news feed.

``{prefix}{url}``

i.e.

Please check out our latest Viewer on Playstore. see http://www.filpal.com

It will fail to generate or broken link display in news feed if you swap the position of the url.

i.e.

Check out our blog http://filpal.wordpress.com for more.

Feed example with url

```json
{
  "entries": [
      {url: "https://www.filpal.com/eds-hf", redirect: "https://www.filpal.com/eds-hf?from=viewer#comp-jzrr9dze1inlineContent", body: "Check out free FILPAL EDS HF Circuit Simulation Tool! Free license download until 30 April 2020! See https://www.filpal.com/eds-hf."}	
]}
```

Feed example without url
```json
{
  "entries": [
	{url: "", redirect: "https://play.google.com/store/apps/details?id=com.filpal.touchstoneviewer", body: "There is also Android version available on Google Playstore. Search on 'touchstone file viewer'."}
]}
```

Example with multiple feed entries
```json
{
  "entries": [
	{url: "https://www.filpal.com/eds-hf", redirect: "https://www.filpal.com/eds-hf?from=viewer#comp-jzrr9dze1inlineContent", body: "Check out free FILPAL EDS HF Circuit Simulation Tool available until end of April 2020! See https://www.filpal.com/eds-hf."},
	{url: "", redirect: "https://play.google.com/store/apps/details?id=com.filpal.touchstoneviewer", body: "There is also Android version available on Google Playstore. Search on 'touchstone file viewer'."},
	{url: "https://www.filpal.com", redirect: "https://www.filpal.com?from=viewer", body: "Check out our homepage for more. See https://www.filpal.com." }
]}
```

Empty feed example
```json
{"entries": []}
```
